package es.vidal.actividad7_2;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Actividad7_2 {

    public static Scanner teclado = new Scanner(System.in);

    public static void main(String[] args) {
        int edad;
        try {
            edad = getEdad();
            System.out.println("La edad del sujeto es : " + edad);
        }catch (InputMismatchException ex){
            System.out.println("El error se ha producido dado que no ha introducido un número entre 10 y 50");
        }
    }

    public static int getEdad(){
        int edad;
        System.out.println("Introduce un número entre el 10 y el 50:");
        edad = teclado.nextInt();
        if (edad < 10 || edad > 50){
            teclado.nextLine();
            throw new InputMismatchException("Error introduce un número entre 10 y 50");
        }
        return edad;
    }
}
