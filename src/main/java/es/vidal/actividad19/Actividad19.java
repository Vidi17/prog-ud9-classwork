package es.vidal.actividad19;

public class Actividad19 {
    public static void main(String[] args) {
        String string = "03420";
        System.out.println(checkCodigoPostal(string));

    }

    public static boolean checkCodigoPostal(String string){
        return string.matches("^([0-4][0-9]|5[0-2])([0-9][0-9][0-9])$");
    }
}
