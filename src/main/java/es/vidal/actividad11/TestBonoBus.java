package es.vidal.actividad11;

import es.vidal.actividad11.exception.SaldoAgotadoException;

public class TestBonoBus {
    public static void main(String[] args) {

        BonoAutobus bonoAutobus = new BonoAutobus();

        for (int i = 0; i < 10; i++) {
            try {
                bonoAutobus.fichar();
                System.out.println("Ha usado un 1 viaje");
            }catch (SaldoAgotadoException ex){
                System.out.println(ex.getMessage());
            }
        }
    }
}
