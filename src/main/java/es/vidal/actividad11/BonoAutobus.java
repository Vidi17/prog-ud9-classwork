package es.vidal.actividad11;

import es.vidal.actividad11.exception.SaldoAgotadoException;

public class BonoAutobus {

    private final int SALDO_VIAJES = 10;

    private int saldo = SALDO_VIAJES;

    public void fichar() throws SaldoAgotadoException {
        if (saldo - 1 <= 0){
            throw new SaldoAgotadoException();
        }
        saldo -= 1;
    }
}
