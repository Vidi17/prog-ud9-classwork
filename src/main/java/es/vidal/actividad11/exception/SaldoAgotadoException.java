package es.vidal.actividad11.exception;

public class SaldoAgotadoException extends Exception{

    public SaldoAgotadoException() {
        super("El saldo de su bono se ha agotado");
    }
}
