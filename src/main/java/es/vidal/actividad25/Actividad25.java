package es.vidal.actividad25;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;

public class Actividad25 {

    public static Scanner input = new Scanner(System.in);

    public static void main(String[] args) {
        LocalDateTime dateTime = LocalDateTime.now();
        System.out.print("""
                1) Añadir días
                2) Añadir horas
                3) Añadir minutos
                Introduce una opción :""");
        int selector = input.nextInt();
        LocalDateTime modifiedDateTime = menuSelector(dateTime, selector);

        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        System.out.println(modifiedDateTime.format(dateTimeFormatter));
    }

    public static LocalDateTime menuSelector(LocalDateTime dateTime, int selector){
        return switch (selector){
            case 1-> addDays(dateTime);
            case 2-> addHours(dateTime);
            case 3-> addMinutes(dateTime);
            default -> null;
        };
    }

    public static LocalDateTime addDays(LocalDateTime dateTime){
        System.out.print("Qué cantidad de días quieres añadir: ");
        int daysToAdd = input.nextInt();
        return dateTime.plusDays(daysToAdd);
    }

    public static LocalDateTime addHours(LocalDateTime dateTime){
        System.out.print("Qué cantidad de horas quieres añadir: ");
        int hoursToAdd = input.nextInt();
        return dateTime.plusHours(hoursToAdd);
    }

    public static LocalDateTime addMinutes(LocalDateTime dateTime){
        System.out.print("Qué cantidad de minutos quieres añadir: ");
        int minutesToAdd = input.nextInt();
        return dateTime.plusMinutes(minutesToAdd);
    }
}
