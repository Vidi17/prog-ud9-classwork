package es.vidal.projecteRestaurant2.validator;

import es.vidal.projecteRestaurant2.date.Fecha;

public class Validator {

    private static final String VALID_DATE = "\\d\\d/\\d\\d/\\d\\d\\d\\d";
    private static final String VALID_NAME = "\\D+";

    public static boolean validateClientName(String name){
        return name.matches(VALID_NAME);
    }

    public static boolean validateDate(String date){
        if (date.matches(VALID_DATE)){
            Fecha validDate = new Fecha(date);
            return validDate.isCorrecta();
        }else {
            return false;
        }
    }

    public static int validateInteger(String number) {
        if (number.matches("^(\\d)|^(\\d)(\\d)$")) {
            return Integer.parseInt(number);
        }else {
            return 0;
        }
    }
}
