package es.vidal.projecteRestaurant2;

import es.vidal.projecteRestaurant2.exceptions.NotFoundException;
import es.vidal.projecteRestaurant2.exceptions.OrderAlreadyServedException;
import es.vidal.projecteRestaurant2.orderLists.OrderHistory;
import es.vidal.projecteRestaurant2.orderLists.PendingOrderQueue;

import java.util.Scanner;

public class Restaurant {

    private final int FIRST_ORDER = 0;
    private final Scanner input = new Scanner(System.in);

    Waiter waiter = new Waiter(new Menu());
    OrderHistory orderHistory = new OrderHistory();
    PendingOrderQueue pendingOrderQueue = new PendingOrderQueue();

    public void addOrder(){
        waiter.setOrder();
        if (waiter.getOrder() != null){
            pendingOrderQueue.addOrder(waiter.getOrder());
            pendingOrderQueue.getPendingOrderList().get(pendingOrderQueue.getPendingOrderList().size() - 1)
                    .setIdOrder("o" + pendingOrderQueue.getPendingOrderList().size());
        }else {
            System.out.println("El pedido introducido está vacío");
        }
    }

    public void markOrder() throws OrderAlreadyServedException {
        if (pendingOrderQueue.getPendingOrderList().isEmpty()) {
            System.out.println("Ahora mismo no hay nigún pedido pendiente de servir. Por favor añade un pedido.\n");
        }else {
            if (pendingOrderQueue.getPendingOrderList().get(FIRST_ORDER).isServed()){
                throw new OrderAlreadyServedException();
            }else {
                Order orderToMark = pendingOrderQueue.getNextOrder();
                System.out.println(orderToMark);
                System.out.println("Su pedido ha sido servido correctamente");
                orderHistory.addOrder(orderToMark);
            }
        }
    }

    public void showOrder(){
        if (orderHistory.getOrderList().isEmpty()){
            System.out.println("Ahora mismo no hay nigún pedido en la lista. Por favor añade un pedido.\n");
        }else {
            listHistory();
            Order orderToShow;
            do {
                try {
                    orderToShow = getOrderToShow();
                }catch (NotFoundException exception){
                    System.out.println(exception.getMessage());
                    orderToShow = null;
                }
                if (orderToShow != null){
                    System.out.println(orderToShow);
                }
            }while (orderToShow != null);
        }
    }

    public Order searchOrder(String code) throws NotFoundException{
        for (Order order: orderHistory.getOrderList()) {
            if (order.getIdOrder().equals(code)){
                return order;
            }
        }
        throw new NotFoundException("Error: El pedido que está buscando no existe");
    }

    public Order getOrderToShow() throws NotFoundException {
        if (orderHistory.getOrderList().isEmpty()){
            System.out.println("Ahora mismo no hay nigún pedido en la lista. Por favor añade un pedido.\n");
            return null;
        }else {
            System.out.println("Introduzca el código del pedido que desea visualizar o introduzca fin para finalizar:");
            boolean validIdOrder;
            Order orderSearched = null;
            String orderToSearch;
            do {
                validIdOrder = input.hasNextInt();
                orderToSearch = input.next();
                if (orderToSearch.equalsIgnoreCase("fin")) {
                    return null;
                }else if (!validIdOrder){
                    orderSearched = searchOrder(orderToSearch);
                    validIdOrder = true;
                }else {
                    System.out.println("Introduzca un código de pedido válido o introduzca fin para finalizar:");
                    validIdOrder = false;
                }
            }while (!validIdOrder);
            return orderSearched;
        }
    }

    public void listHistory(){
        if (orderHistory.getOrderList().isEmpty()){
            System.out.println("Ahora mismo no hay nigún pedido en la lista. Por favor añade un pedido.\n");
        }else {
            System.out.println(orderHistory);
            System.out.println();
        }
    }

}
