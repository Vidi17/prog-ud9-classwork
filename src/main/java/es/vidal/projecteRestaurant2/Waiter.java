package es.vidal.projecteRestaurant2;

import java.util.InputMismatchException;
import java.util.LinkedList;
import java.util.Scanner;

import es.vidal.projecteRestaurant2.date.Fecha;
import es.vidal.projecteRestaurant2.exceptions.NotFoundException;
import es.vidal.projecteRestaurant2.validator.Validator;
import es.vidal.projecteRestaurant2.productTypes.Dessert;
import es.vidal.projecteRestaurant2.productTypes.Drink;
import es.vidal.projecteRestaurant2.productTypes.Sandwich;
import es.vidal.projecteRestaurant2.productTypes.Starter;
import es.vidal.projecteRestaurant2.questions.QuestionMaker;

public class Waiter {

    private final Scanner input = new Scanner(System.in);

    private final Menu menu;

    private Order order;

    public Waiter(Menu menu) {
        this.menu = menu;
    }

    public void setOrder(){
        order = createOrder();
    }

    public Order getOrder(){
        return order;
    }

    public Order createOrder(){
        String clientName = getClientName();
        Fecha date = new Fecha();
        boolean validDate = false;
        do {
            try {
                date = getDate();
                validDate = true;
            }catch (InputMismatchException ex){
                System.out.println(ex.getMessage());
            }
        }while (!validDate);
        Order order = new Order(clientName, date);
        LinkedList<OrderLine> orderLines = getOrderLineListForOrder();
        if (orderLines == null){
            return null;
        }else {
            order.setProductList(orderLines);
            System.out.println(order);
            return order;
        }
    }

    public String getClientName(){
        System.out.println("Introduzca su nombre:");
        String clientName;
        boolean validClientName;
        do {
             clientName = input.next();
            validClientName = Validator.validateClientName(clientName);
            if (!validClientName) {
                System.out.println("Introduzca un nombre válido:");
                input.nextLine();
            }
        }while (!validClientName);
        return clientName;
    }

    public Fecha getDate() throws InputMismatchException {
        System.out.println("Introduzca la fecha actual en formato dd/mm/yyyy:");
        boolean validDate;
        String dateToAdd;
        dateToAdd = input.next();
        validDate = Validator.validateDate(dateToAdd);
        if (!validDate) {
            input.nextLine();
            throw new InputMismatchException("Error: La fecha introducida no es válida");
        } else {
            return new Fecha(dateToAdd);
        }
    }

    public LinkedList<OrderLine> getOrderLineListForOrder(){
        LinkedList<OrderLine> orderProducts = new LinkedList<>();
        LinkedList<OrderLine> drinks = getOrderLine(Drink.class);
        LinkedList<OrderLine> starters = getOrderLine(Starter.class);
        LinkedList<OrderLine> sandwiches = getOrderLine(Sandwich.class);
        LinkedList<OrderLine> desserts = getOrderLine(Dessert.class);
        if (drinks != null){
            orderProducts.addAll(drinks);
        }
        if (starters != null){
            orderProducts.addAll(starters);
        }
        if (sandwiches != null){
            orderProducts.addAll(sandwiches);
        }
        if (desserts != null){
            orderProducts.addAll(desserts);
        }
        if (orderProducts.isEmpty()){
            return null;
        }else {
            return orderProducts;
        }
    }

    public LinkedList<OrderLine> getOrderLine(Class type){
        QuestionMaker questionMaker = new QuestionMaker();
        LinkedList<OrderLine> productList = new LinkedList<>();
        questionMaker.askFirst(type);
        String addProduct = input.next();
        int quantity;
        if (!validateChoice(addProduct)){
            input.nextLine();
            return null;
        }else {
            menu.viewProducts(type);
            boolean wantMore;
            do {
                Product productToAdd = getProductForOrderLine(type);
                if (productToAdd != null){
                    quantity = getQuantity();
                    productList.add(new OrderLine(productToAdd, quantity));
                    System.out.println("Añadido");
                    questionMaker.askWantMore(type);
                    String addMore = input.next();
                    wantMore = validateChoice(addMore);
                }else {
                    wantMore = false;
                }
            }while (wantMore);
            return productList;
        }
    }

    public boolean validateChoice(String string){
        return string.matches("Si|si|SI");
    }

    public Product getProductForOrderLine(Class type){
        QuestionMaker questionMaker = new QuestionMaker();
        questionMaker.askForCode(type);
        boolean validProductName;
        Product productSearched = null;
        String productToSearch;
        do{
            do {
                validProductName = input.hasNextInt();
                productToSearch = input.next();
                if (productToSearch.equalsIgnoreCase("fin")) {
                    return null;
                }else if (!validProductName){
                    try {
                        productSearched = menu.searchProduct(productToSearch, type);
                        validProductName = true;
                    }catch (NotFoundException exception){
                        System.out.println(exception.getMessage());
                    }
                }else {
                    questionMaker.askForValid(type);
                    validProductName = false;
                }
            }while (!validProductName);
            if (productSearched == null){
                questionMaker.askForValid(type);
            }
        }while (productSearched == null);
        return productSearched;
    }

    public int getQuantity(){
        System.out.println("¿Cuantas unidades quieres añadir de este producto?:");
        int validNumber;
        do {
            String number = input.next();
            validNumber = Validator.validateInteger(number);
            if (validNumber == 0){
                System.out.println("Introduce un número válido:");
                input.nextLine();
            }
        }while (validNumber == 0);
        return validNumber;
    }



}
