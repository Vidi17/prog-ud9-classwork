package es.vidal.projecteRestaurant2.exceptions;

public class FullListException extends RuntimeException{
    public FullListException() {
        super("La cola de pedidos està llena");
    }
}
