package es.vidal.projecteRestaurant2.exceptions;

public class NotAplicableDiscount extends RuntimeException{
    public NotAplicableDiscount() {
        super("Los postres no pueden tener descuento");
    }
}
