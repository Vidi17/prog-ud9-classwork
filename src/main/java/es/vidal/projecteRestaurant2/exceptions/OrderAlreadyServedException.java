package es.vidal.projecteRestaurant2.exceptions;

public class OrderAlreadyServedException extends RuntimeException{
    public OrderAlreadyServedException() {
        super("Error: El pedido seleccionado ya ha sido servido");
    }
}
