package es.vidal.projecteRestaurant2.questions;

import es.vidal.projecteRestaurant2.productTypes.Drink;
import es.vidal.projecteRestaurant2.Product;
import es.vidal.projecteRestaurant2.productTypes.Sandwich;
import es.vidal.projecteRestaurant2.productTypes.Starter;

public class QuestionMaker {

    public void askForCode(Class<Product> type){
        if (type.equals(Drink.class)){
            System.out.println("Introduzca el código de la bebida que desea tomar o introduzca fin para finalizar: ");
        }else if (type.equals(Sandwich.class)){
            System.out.println("Introduzca el código del bocadillo que desea tomar o introduzca fin para finalizar: ");
        }else if (type.equals(Starter.class)){
            System.out.println("Introduzca el código del entrante que desea tomar o introduzca fin para finalizar: ");
        }else {
            System.out.println("Introduzca el código del postre que desea tomar o introduzca fin para finalizar: ");
        }
    }

    public void askWantMore(Class<Product> type){
        if (type.equals(Drink.class)){
            System.out.println("¿Quiere añadir más bebidas a su pedido? [Si][No]");
        }else if (type.equals(Sandwich.class)){
            System.out.println("¿Quiere añadir más bocadillos a su pedido? [Si][No]");
        }else if (type.equals(Starter.class)){
            System.out.println("¿Quiere añadir más entrantes a su pedido? [Si][No]");
        }else {
            System.out.println("¿Quiere añadir más postres a su pedido? [Si][No]");
        }
    }

    public void askFirst(Class<Product> type){
        if (type.equals(Drink.class)){
            System.out.println("¿Desea tomar alguna bebida? [Si][No]");
        }else if (type.equals(Sandwich.class)){
            System.out.println("¿Desea pedir algún bocadillo? [Si][No]");
        }else if (type.equals(Starter.class)){
            System.out.println("¿Desea tomar algún entrante? [Si][No]");
        }else {
            System.out.println("¿Desea tomar algun postre? [Si][No]");
        }
    }

    public void askForValid(Class<Product> type){
        if (type.equals(Drink.class)){
            System.out.println("Introduzca un código de bebida válido o introduzca fin para finalizar:");
        }else if (type.equals(Sandwich.class)){
            System.out.println("Introduzca un código de bocadillo válido o introduzca fin para finalizar:");
        }else if (type.equals(Starter.class)){
            System.out.println("Introduzca un código de entrante válido o introduzca fin para finalizar:");
        }else {
            System.out.println("Introduzca un código de postre válido o introduzca fin para finalizar:");
        }
    }
}
