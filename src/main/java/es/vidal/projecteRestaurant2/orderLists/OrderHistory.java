package es.vidal.projecteRestaurant2.orderLists;

import de.vandermeer.asciitable.AsciiTable;
import de.vandermeer.skb.interfaces.transformers.textformat.TextAlignment;
import es.vidal.projecteRestaurant2.Order;
import es.vidal.projecteRestaurant2.exceptions.FullListException;
import java.util.LinkedList;

public class OrderHistory {

    private final int ORDER_LIMIT = 101;

    private final LinkedList<Order> orderList = new LinkedList<>();

    public void addOrder(Order order) throws FullListException {
        if (isFull()){
            throw new FullListException();
        }else {
            orderList.add(order);
        }
    }

    public boolean isFull(){
        return (orderList.size() == ORDER_LIMIT);
    }

    public String createTable(){
        AsciiTable asciiTable = new AsciiTable();
        asciiTable.addRule();
        asciiTable.addRow("Código", "Nombre", "Fecha", "Estado");
        asciiTable.addRule();
        for (Order order: orderList) {
            if (order.isServed()){
                asciiTable.addRow(order.getIdOrder(), order.getClientName(), order.getOrderDate()
                        , "Servido");
            }else {
                asciiTable.addRow(order.getIdOrder(), order.getClientName(), order.getOrderDate()
                        , "No servido");
            }
            asciiTable.addRule();
        }
        asciiTable.addRow("#", "#", "#", "#");
        asciiTable.setTextAlignment(TextAlignment.RIGHT);
        asciiTable.addRule();
        return asciiTable.render(110);
    }

    public LinkedList<Order> getOrderList() {
        return orderList;
    }

    @Override
    public String toString() {
        return createTable();
    }
}
