package es.vidal.projecteRestaurant2.orderLists;

import java.util.LinkedList;

import de.vandermeer.asciitable.AsciiTable;
import de.vandermeer.skb.interfaces.transformers.textformat.TextAlignment;
import es.vidal.projecteRestaurant2.Order;
import es.vidal.projecteRestaurant2.exceptions.FullListException;

public class PendingOrderQueue {

    private final int ORDER_LIMIT = 101;
    private final int FIRST_ORDER = 0;

    private final LinkedList<Order> pendingOrderList = new LinkedList<>();

    public void addOrder(Order order) throws FullListException{
        if (isFull()){
            throw new FullListException();
        }else {
            pendingOrderList.add(order);
        }
    }

    public boolean isFull(){
        return (pendingOrderList.size() == ORDER_LIMIT);
    }

    public Order getNextOrder(){
        if (pendingOrderList.get(FIRST_ORDER) != null){
            Order aux = pendingOrderList.get(FIRST_ORDER);
            aux.setServed(true);
            pendingOrderList.remove(FIRST_ORDER);
            return aux;
        }else {
            return null;
        }
    }

    public LinkedList<Order> getPendingOrderList() {
        return pendingOrderList;
    }

    public String createTable(){
        AsciiTable asciiTable = new AsciiTable();
        asciiTable.addRule();
        asciiTable.addRow("Código", "Nombre", "Fecha", "Estado");
        asciiTable.addRule();
        for (Order order: pendingOrderList) {
            if (order.isServed()){
                asciiTable.addRow(order.getIdOrder(), order.getClientName(), order.getOrderDate()
                        , "Servido");
            }else {
                asciiTable.addRow(order.getIdOrder(), order.getClientName(), order.getOrderDate()
                        , "No servido");
            }
            asciiTable.addRule();
        }
        asciiTable.addRow("#", "#", "#", "#");
        asciiTable.setTextAlignment(TextAlignment.RIGHT);
        asciiTable.addRule();
        return asciiTable.render(110);
    }

    @Override
    public String toString() {
        return createTable();
    }
}
