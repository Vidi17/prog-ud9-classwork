package es.vidal.projecteRestaurant2;

import es.vidal.projecteRestaurant2.exceptions.OrderAlreadyServedException;
import es.vidal.projecteRestaurant2.validator.Validator;
import es.vidal.projecteRestaurant2.login.Login;

import java.util.InputMismatchException;
import java.util.Scanner;

public class MenuApp {

    private final Scanner input = new Scanner(System.in);

    private final int CREATE_NEW_ORDER = 1;
    private final int LIST_ALL_ORDERS = 2;
    private final int SHOW_ORDER = 3;
    private final int SERVE_ORDER = 4;
    private final int EXIT = 5;

    public void show(){
        menu();
    }

    public void menu(){
        Login accesControl = new Login();
        accesControl.fillUsers();
        Restaurant restaurant = new Restaurant();
        boolean isWorking;
        if (accesControl.verifyUser()){
            System.out.println("""
                    ==============================================
                    === Bienvenido al bar de los 20 Montaditos ===
                    ==============================================""");
            do {
                isWorking = selectType(chooseType(), restaurant);
            }while (isWorking);
        }
        System.out.println("¡Hasta otra!");
    }

    public boolean selectType(int selectType, Restaurant restaurant){
        switch (selectType) {
            case CREATE_NEW_ORDER -> {
                restaurant.addOrder();
                return true;
            }
            case LIST_ALL_ORDERS -> {
                restaurant.listHistory();
                return true;
            }
            case SHOW_ORDER -> {
                restaurant.showOrder();
                return true;
            }
            case SERVE_ORDER -> {
                try {
                    restaurant.markOrder();
                    return true;
                }catch (OrderAlreadyServedException ex){
                    System.out.println(ex.getMessage());
                    return true;
                }
            }
            case EXIT -> {
                return false;
            }
        }
        return false;
    }

    public int chooseType(){
        System.out.printf("""
            %d - Crear nuevo pedido
            %d - Listar todos los pedidos
            %d - Visualizar pedido
            %d - Preparar pedido
            %d - Salir

            Seleccione una opción:\s""", CREATE_NEW_ORDER, LIST_ALL_ORDERS, SHOW_ORDER, SERVE_ORDER
        , EXIT);
        int selectType;

        do {
            selectType = Validator.validateInteger(input.nextLine());
            if (selectType < 1 || selectType > 5){
                System.out.print("Por favor seleccione una opción válida: ");
            }
        }while (selectType < 1 || selectType > 5);
        System.out.println();
        return selectType;
    }
}
