package es.vidal.projecteRestaurant2.views;

import java.text.DecimalFormat;
import java.util.LinkedList;
import de.vandermeer.asciitable.AsciiTable;
import de.vandermeer.skb.interfaces.transformers.textformat.TextAlignment;
import es.vidal.projecteRestaurant2.Product;

public class ProductListView {

    private final LinkedList<Product> productList;

    public ProductListView(LinkedList<Product> productList) {
        this.productList = productList;
    }

    private String renderView() {
        DecimalFormat df = new DecimalFormat("0.00");
        AsciiTable asciiTable = new AsciiTable();
        asciiTable.addRule();
        asciiTable.addRow("Código", null, null, "Descripción", "Precio", "Descuento");
        asciiTable.addRule();
        for (Product product: productList) {
            asciiTable.addRow(product.getCodProduct(), null, null, product.getDescription()
                    , df.format(product.getSalePrice()) + "€", product.getDiscount() + "%");
            asciiTable.addRule();
        }
        asciiTable.addRow("#", "#", "#", "#", "#", "#");
        asciiTable.setTextAlignment(TextAlignment.RIGHT);
        asciiTable.addRule();
        return asciiTable.render(110);
    }

    public String toString() {
        return renderView();
    }
}
