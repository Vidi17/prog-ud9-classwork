package es.vidal.projecteRestaurant2;

import es.vidal.projecteRestaurant2.exceptions.NotAplicableDiscount;
import es.vidal.projecteRestaurant2.exceptions.NotFoundException;
import es.vidal.projecteRestaurant2.productTypes.*;
import es.vidal.projecteRestaurant2.views.ProductListView;
import java.util.HashSet;
import java.util.LinkedList;


public class Menu {

    HashSet<Product> productList = new HashSet<>();

    public Menu() {

        try {
            productList.add(new Sandwich("Kebab mixto dos salsas", 3.00, 5));
            productList.add(new Sandwich("Bocata vegetal con pollo", 2.50, 10));
            productList.add(new Sandwich("Bocata vegetal con lechuga, tomate y queso", 2.75
                    , 0));
            productList.add(new Sandwich("Hamburguesa con bacon ahumado, cebolla crujiente y allioli"
                    , 4.00, 0));
            productList.add(new Sandwich("Burguer MAX DIABLO TRIPLE X", 7.00, 7));
            productList.add(new Drink("Coca Cola Grande Rellenable", 2.50, 0, SizeTypes.BIG
                    , true));
            productList.add(new Drink("Nestea Mediano No Rellenable"
                    , 1.78, 0, SizeTypes.MEDIUM, false));
            productList.add(new Drink("Tanque de Cerveza"
                    , 2.55, 5, SizeTypes.BIG, false));
            productList.add(new Drink("Fanta de Limón Pequeña Rellenable", 1.20, 0
                    , SizeTypes.SMALL, true));
            productList.add(new Drink("Agua Mineral Mediana", 0.90, 0, SizeTypes.MEDIUM
                    , false));
            productList.add(new Dessert("Coulant de Chocolate"
                    , 3.30, 0));
            productList.add(new Dessert("Helado de Limón", 2.45, 0));
            productList.add(new Dessert("Tarrina de Mantecao de ca Pana", 4.00, 0));
            productList.add(new Dessert("Copa Especial de la Casa", 6.15, 0));
            productList.add(new Dessert("Pirulo Jungli", 2.00, 0));
            productList.add(new Starter("Bandeja de Embutidos", 0.80, 0, 4));
            productList.add(new Starter("ALBÓNDIGAS con salsa BBQ", 0.70, 25, 6));
            productList.add(new Starter("Patatas Rancheras con Bacon Huevo y Queso", 3.00, 10
                    , 1));
            productList.add(new Starter("Xipirones a la Marinara", 0.70, 30, 8));
            productList.add(new Starter("Pinchos de tortilla de patatas", 0.90, 5
                    , 4));
            genCodProducts();
        }catch (NotAplicableDiscount ex){
            System.out.println(ex.getMessage());
        }
    }

    public void genCodProducts(){
        int counter = 1;
        for (Product product: productList) {
            product.setCodProduct("p" + counter);
            counter++;
        }
    }

    public Product searchProduct(String code, Class<Product> tipo) throws NotFoundException {
        for (Product product: productList) {
            if (product.getCodProduct().equals(code)){
                if (product.getClass() == tipo){
                    System.out.print(product);
                    return product;
                }
            }
        }
        throw new NotFoundException("Error: El producto buscado no existe");
    }

    public void viewProducts(Class type){
        LinkedList<Product> productListView = new LinkedList<>();
        for (Product product: productList) {
            if (product.getClass() == type){
               productListView.add(product);
            }
        }
        System.out.println(new ProductListView(productListView));
    }
}
