package es.vidal.projecteRestaurant2.productTypes;

import es.vidal.projecteRestaurant2.Product;
import es.vidal.projecteRestaurant2.exceptions.NotAplicableDiscount;

public class Dessert extends Product {

    public Dessert(String description, double basePrice, double discount) {
        super(description, basePrice, discount);
        setDiscount(discount);
    }

    @Override
    public void setDiscount(double discount) throws NotAplicableDiscount{
        if (discount != 0){
            throw new NotAplicableDiscount();
        }
        super.setDiscount(discount);
    }
}
