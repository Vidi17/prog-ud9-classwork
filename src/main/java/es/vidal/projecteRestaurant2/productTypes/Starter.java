package es.vidal.projecteRestaurant2.productTypes;

import es.vidal.projecteRestaurant2.Product;

public class Starter extends Product {

    private final int servingsNumber;

    public Starter(String description, double basePrice, double discount, int servingsNumber) {
        super(description, basePrice, discount);
        this.servingsNumber = servingsNumber;
    }

    @Override
    public double getSalePrice() {
        return super.getSalePrice() + servingsNumber - (super.getSalePrice() + servingsNumber)
                * (super.getDiscount() / 100);
    }
}
