package es.vidal.projecteRestaurant2.productTypes;

import es.vidal.projecteRestaurant2.Product;

public class Drink extends Product {

    private final SizeTypes size;

    private final boolean refillable;

    public Drink(String description, double basePrice, double discount, SizeTypes size, boolean refillable) {
        super(description, basePrice, discount);
        this.size = size;
        this.refillable = refillable;
    }
}
