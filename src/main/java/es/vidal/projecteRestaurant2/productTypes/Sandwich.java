package es.vidal.projecteRestaurant2.productTypes;

import es.vidal.projecteRestaurant2.Product;

public class Sandwich extends Product {

    public Sandwich(String description, double basePrice, double discount) {
        super(description, basePrice, discount);
    }
}
