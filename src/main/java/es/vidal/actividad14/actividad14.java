package es.vidal.actividad14;

import java.util.Scanner;

public class actividad14 {

    public static Scanner teclado = new Scanner(System.in);

    public static void main(String[] args) {

        String hora = getHora();
        System.out.println(checkFormatoHora(hora));

    }

    public static String getHora(){
        System.out.println("Introduce la hora:");
        return teclado.nextLine();
    }

    public static boolean checkFormatoHora(String hora){

        return hora.matches("^(1[012]|0[0-9]):[0-5][0-9]\\s(am|pm)$");
    }
}
