package es.vidal.repasoExamen;

import es.vidal.repasoExamen.exceptions.ShowRoomNotOpenException;
import es.vidal.repasoExamen.showcaseTypes.TemporalShowcaseWhileStockLast;

import java.util.LinkedList;

public class TestShopOnline {
    public static void main(String[] args) {

        Product product1 = new Product("polo rayas", 50.00, Brand.LaCostera);
        Product product2 = new Product("bañador azul", 40.00, Brand.LaCostera);
        LinkedList<Product> products = new LinkedList<>();

        products.add(product1);
        products.add(product2);

        TemporalShowcaseWhileStockLast temporalShowcase = new TemporalShowcaseWhileStockLast(
                "verano LaCostera 2020", Brand.LaCostera, products);


        try {
            temporalShowcase.consultProductsAlphabetically();
            temporalShowcase.consultNumberOfProducts(product1);
            temporalShowcase.consultNumberOfProducts(product2);
            temporalShowcase.checkMillisecondsLeftToClose();
            temporalShowcase.buyProduct(temporalShowcase.getProductsStock().get(0).getQuantity(), product2);
            temporalShowcase.buyProduct(temporalShowcase.getProductsStock().get(1).getQuantity(), product1);
            if (temporalShowcase.isOpen()){
                System.out.println("El escaparate está abierto");
            }else {
                System.out.println("El escaparate está cerrado");
            }
        }catch (ShowRoomNotOpenException exception){
            System.out.println(exception.getMessage());
        }
    }
}
