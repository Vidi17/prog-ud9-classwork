package es.vidal.repasoExamen.showcaseTypes;

import es.vidal.repasoExamen.Brand;
import es.vidal.repasoExamen.Product;
import es.vidal.repasoExamen.Showcase;
import es.vidal.repasoExamen.exceptions.ShowRoomNotOpenException;

import java.util.LinkedList;

public class TemporalShowcase extends Showcase {

    private final int MILLISECONDS_PER_SECOND = 1000;
    private final int SECONDS_PER_DAY = 86400;

    private final int daysOpen;

    private final long millisecondsWhenOpen;

    public TemporalShowcase(String name, Brand brand, int daysOpen, LinkedList<Product> products) {
        super(name, brand, products);
        this.daysOpen = daysOpen;
        millisecondsWhenOpen = System.currentTimeMillis();
    }

    public void checkMillisecondsLeftToClose() throws ShowRoomNotOpenException{
        if (!super.isOpen()){
            throw new ShowRoomNotOpenException();
        }
        double totalMilliseconds = daysOpen * SECONDS_PER_DAY * MILLISECONDS_PER_SECOND;
        if (totalMilliseconds - (System.currentTimeMillis() - millisecondsWhenOpen) <= 0){
            closeShowcase();
        }
        System.out.println("Quedan " + millisecondsToHours(totalMilliseconds) + " Horas para que cierre la tienda");
    }

    public int millisecondsToHours(double milliseconds){
        return (int) (((milliseconds / MILLISECONDS_PER_SECOND) / 60) / 60);
    }
}
