package es.vidal.repasoExamen.showcaseTypes;

import es.vidal.repasoExamen.Brand;
import es.vidal.repasoExamen.Product;

import java.util.LinkedList;

public class TemporalShowcaseWhileStockLast extends TemporalShowcase {

    public TemporalShowcaseWhileStockLast(String name, Brand brand, LinkedList<Product> products) {
        super(name, brand, 3, products);
    }

    public void checkShowcase(){
        if (super.getProductsStock().isEmpty()){
            closeShowcase();
        }
    }
}
