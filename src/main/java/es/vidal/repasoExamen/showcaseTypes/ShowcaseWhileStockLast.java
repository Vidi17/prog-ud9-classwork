package es.vidal.repasoExamen.showcaseTypes;

import es.vidal.repasoExamen.Brand;
import es.vidal.repasoExamen.Product;
import es.vidal.repasoExamen.Showcase;

import java.util.LinkedList;

public class ShowcaseWhileStockLast extends Showcase {

    public ShowcaseWhileStockLast(String name, Brand brand, LinkedList<Product> products) {
        super(name, brand, products);
    }

    public void checkShowcase(){
        if (super.getProductsStock().isEmpty()){
            closeShowcase();
        }
    }

    @Override
    public void closeShowcase() {
        super.closeShowcase();
    }
}
