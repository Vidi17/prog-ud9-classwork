package es.vidal.repasoExamen;

import es.vidal.repasoExamen.exceptions.CanNotMergeBrandsException;
import es.vidal.repasoExamen.exceptions.NotExistEnoughItemException;
import es.vidal.repasoExamen.exceptions.ShowRoomNotOpenException;

import java.util.LinkedList;
import java.util.Random;

public abstract class Showcase {

    private final int LIMIT_PRODUCTS_SALE = 20;

    private final String name;

    private final Brand brand;

    private final LinkedList<ProductStock> productsStock = new LinkedList<>();

    private final LinkedList<Product> products;

    private boolean isOpen;

    private final Random randomNumber = new Random();

    public Showcase(String name, Brand brand, LinkedList<Product> products) {
        this.name = name;
        this.brand = brand;
        this.products = products;
        isOpen = true;
        try {
            fillProductStock();
        }catch (CanNotMergeBrandsException ex){
            System.out.println(ex.getMessage());
        }
    }

    public void fillProductStock() throws CanNotMergeBrandsException{
        for (int i = 0; i < LIMIT_PRODUCTS_SALE; i++) {
            if (products.size() == i){
                return;
            }else if (products.get(i).getBrand() != brand){
                throw new CanNotMergeBrandsException();
            }else {
                productsStock.add(new ProductStock(products.get(i), randomNumber.nextInt(10)+1));
            }
        }
    }

    public void consultSaleProducts(){
        System.out.println("Listado de productos a la venta:");
        for (int i = 0; i < productsStock.size(); i++) {
            if (productsStock.get(i).getQuantity() > 0){
                System.out.println(productsStock);
            }
        }
    }

    public void consultProductsAlphabetically() throws ShowRoomNotOpenException{
        if (!isOpen){
            throw new ShowRoomNotOpenException();
        }
        sortProducts();
        for (ProductStock productStock : productsStock) {
            System.out.println(productStock);
        }
    }

    public void sortProducts(){
        for (int i = 0; i < productsStock.size(); i++) {
            for (int j = 0; j < productsStock.size(); j++) {
                if (productsStock.get(i).compareTo(productsStock.get(j)) < 0){
                    substituteProduct(productsStock.get(i), productsStock.get(j), i, j);
                }
            }
        }
    }

    public void substituteProduct(ProductStock p1, ProductStock p2, int index, int index2){
        productsStock.set(index, p2);
        productsStock.set(index2, p1);
    }

    public boolean consultIsAvailableNumberOfProducts(int quantity, Product product){
        for (ProductStock productStock : productsStock) {
            if (productStock.getProduct().equals(product) && quantity == productStock.getQuantity()) {
                return true;
            }
        }
        return false;
    }

    public boolean consultIsAvailableProduct(Product product){
        for (ProductStock productStock : productsStock) {
            if (productStock.getProduct().equals(product) && productStock.getQuantity() > 0) {
                return true;
            }
        }
        return false;
    }

    public void consultNumberOfProducts(Product product) throws ShowRoomNotOpenException{
        if (!isOpen){
            throw new ShowRoomNotOpenException();
        }
        for (ProductStock productStock : productsStock) {
            if (productStock.getProduct().equals(product)){
                System.out.println("La cantidad de " + productStock.getProduct().getIdentifier() +
                        " es: " + productStock.getQuantity());
            }
        }
    }

    public LinkedList<ProductStock> getProductsStock() {
        return productsStock;
    }

    public void closeShowcase(){
        setOpen(false);
    }

    public boolean isOpen() {
        return isOpen;
    }

    public void setOpen(boolean open) {
        isOpen = open;
    }

    public void buyProduct(int quantity, Product product) throws NotExistEnoughItemException, ShowRoomNotOpenException{
        if (!isOpen){
            throw new ShowRoomNotOpenException();
        }
        for (ProductStock productStock : productsStock) {
            if (productStock.getProduct().equals(product) && productStock.getQuantity() < quantity){
                throw new NotExistEnoughItemException();
            }else if (productStock.getProduct().equals(product)){
                productStock.setQuantity(productStock.getQuantity() - quantity);
                System.out.println("La compra se ha realizado con éxito");
            }
        }
    }
}
