package es.vidal.repasoExamen;

public class ProductStock implements Comparable<Object>{

    private final Product product;
    private int quantity;

    public ProductStock(Product product, int quantity) {
        this.product = product;
        this.quantity = quantity;
    }

    public Product getProduct() {
        return product;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    @Override
    public String toString() {
        return  "Producto = " + product +
                ", Cantidad = " + quantity;
    }

    @Override
    public int compareTo(Object o) {
        ProductStock o1 = (ProductStock) o;
        return this.getProduct().getIdentifier().compareTo(o1.getProduct().getIdentifier());
    }
}
