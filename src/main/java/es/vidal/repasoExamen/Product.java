package es.vidal.repasoExamen;

import java.text.DecimalFormat;
import java.util.Objects;

public class Product {

    private final String identifier;

    private double price;

    private final Brand brand;

    public Product(String identifier, double price, Brand brand) {
        this.identifier = identifier;
        this.price = price;
        this.brand = brand;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getIdentifier() {
        return identifier;
    }

    public double getPrice() {
        return price;
    }

    public Brand getBrand() {
        return brand;
    }

    @Override
    public String toString() {
        DecimalFormat df = new DecimalFormat("0.00");
        return "{Identificador = " + identifier + '\'' +
                ", Precio = " + df.format(price) +
                ", Marca = " + brand + '}';
    }

    @Override
    public boolean equals(Object o) {
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Product product = (Product) o;
        return identifier.equals(product.identifier);
    }

    @Override
    public int hashCode() {
        return Objects.hash(identifier);
    }
}
