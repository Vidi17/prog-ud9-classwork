package es.vidal.repasoExamen.exceptions;

public class ShowRoomNotOpenException extends Exception{
    public ShowRoomNotOpenException() {
        super("El escaparate no está abierto");
    }
}
