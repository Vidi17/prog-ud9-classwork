package es.vidal.repasoExamen.exceptions;

public class CanNotMergeBrandsException extends RuntimeException{
    public CanNotMergeBrandsException() {
        super("Los productos no són de la misma marca que el escaparate");
    }
}
