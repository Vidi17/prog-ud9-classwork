package es.vidal.repasoExamen.exceptions;

public class NotExistEnoughItemException extends RuntimeException{
    public NotExistEnoughItemException() {
        super("No hay suficientes unidades del producto que desea comprar");
    }
}
