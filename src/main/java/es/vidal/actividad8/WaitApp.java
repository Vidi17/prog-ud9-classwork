package es.vidal.actividad8;

public class WaitApp {
    public static void main(String[] args) {
        try {
            waitSeconds(10);
            System.out.println("Ha finalizado la cuenta de segundos");
        }catch (InterruptedException ex){
            System.out.println(ex.getMessage());
        }
    }

    public static void waitSeconds(int seconds) throws InterruptedException{

        Thread.sleep(seconds * 1000L);

    }
}
