package es.vidal.actividad10.exception;

public class ApareamientoInvalidoException extends Exception{

    public ApareamientoInvalidoException() {
        super("El apareamiento entre animales que no són de la misma raza no es posible");
    }
}
