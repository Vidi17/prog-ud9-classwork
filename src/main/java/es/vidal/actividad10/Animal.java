package es.vidal.actividad10;

import es.vidal.actividad10.exception.ApareamientoInvalidoException;

abstract public class Animal {

    protected boolean vacunado;

    enum Comida{HERBIVORO, CARNIVORO, OMNIVORO}

    public Comida comida;

    protected int hambre;

    enum Tamanyo{GRANDE, PEQUENYO, MEDIANO}

    public Tamanyo tamanyo;

    protected String localizacion;

    private Animal pareja;

    public abstract void emitirSonido();

    public void comer(){
        hambre = 0;
    }

    public void vacunar(){
        vacunado = true;
        System.out.println("Vacunando...");
    }

    public void aparearConAnimal(Animal animal) throws ApareamientoInvalidoException{
        if (animal.getClass() != this.getClass()){
            throw new ApareamientoInvalidoException();
        }
        pareja = animal;
    }

    public String toString(){
        return "";
    }
}


