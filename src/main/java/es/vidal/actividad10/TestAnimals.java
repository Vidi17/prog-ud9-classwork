package es.vidal.actividad10;

import es.vidal.actividad10.exception.ApareamientoInvalidoException;
public class TestAnimals {

    public static void main(String[] args) {

        Animal leon1 = new Leon(Animal.Tamanyo.GRANDE,"La Sabana");

        Animal leon2 = new Leon(Animal.Tamanyo.GRANDE, "La sabana");

        Animal lobo1 = new Lobo(Animal.Tamanyo.GRANDE,"La Sabana");

        Animal lobo2 = new Lobo(Animal.Tamanyo.MEDIANO,"La Serreta");

        Animal perro1 = new Perro(Animal.Tamanyo.PEQUENYO,"Alcoy");

        Animal perro2 = new Perro(Animal.Tamanyo.PEQUENYO,"Castalla");

        Animal gato1 = new Gato(Animal.Tamanyo.PEQUENYO, "Onil");

        Animal gato2 = new Gato(Animal.Tamanyo.PEQUENYO, "Onil");

        Animal hipopotamo1 = new Hipopotamo(Animal.Tamanyo.GRANDE, "Ibi");

        Animal hipopotamo2 = new Hipopotamo(Animal.Tamanyo.GRANDE, "León");

        Animal tigre1 = new Tigre(Animal.Tamanyo.MEDIANO, "La sabana");

        Animal tigre2 = new Tigre(Animal.Tamanyo.MEDIANO, "Tibi");

        generarApareamientos(leon1, leon2);
        generarApareamientos(leon1, perro2);
        generarApareamientos(perro1, perro2);
        generarApareamientos(gato1, gato2);
        generarApareamientos(gato1, hipopotamo2);
        generarApareamientos(hipopotamo1, hipopotamo2);
        generarApareamientos(tigre1, tigre2);
        generarApareamientos(tigre1, lobo2);
        generarApareamientos(lobo1, lobo2);

    }

    public static void generarApareamientos(Animal animal1 , Animal animal2){
        try {
            animal1.aparearConAnimal(animal2);
            System.out.println("Los animales se han apareado con éxito");
        }catch (ApareamientoInvalidoException ex){
            System.out.println(ex.getMessage());
        }
    }
}
