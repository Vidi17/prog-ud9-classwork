package es.vidal.actividad1;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Actividad1 {

    public static Scanner teclado = new Scanner(System.in);
    public static void main(String[] args) {

        ArrayList<Integer> numeros = new ArrayList<>();

        llenarArray(numeros);

        System.out.printf("El número más alto es %d", sacarElMaximo(numeros));
    }

    public static void llenarArray(ArrayList<Integer> numeros){
        for (int i = 0; i < 6; i++) {
            try {
                System.out.printf("Introduce el número %d: ", i + 1);
                int numero = teclado.nextInt();
                numeros.add(numero);
            }catch (InputMismatchException ex){
                System.out.println("Debe ingresar obligatoriamente un número entero");
                teclado.nextLine();
                i--;
            }
        }
    }

    public static int sacarElMaximo(ArrayList<Integer> numeros){
        int numero = numeros.get(0);
        for (Integer integer : numeros) {
            if (integer > numero) {
                numero = integer;
            }
        }
        return numero;
    }
}
