package es.vidal.actividad23;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Scanner;

public class Actividad23 {

    public static Scanner input = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("Introduce una fecha en formato (YYYY/MM/DD hh:mm:ss)");
        String stringDateTime = input.nextLine();

        try {
            DateTimeFormatter entryFormat = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
            LocalDateTime dateTime = LocalDateTime.parse(stringDateTime, entryFormat);

            DateTimeFormatter exitFormat = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");
            System.out.println("La fecha y hora es: " + dateTime.format(exitFormat));
        }catch (DateTimeParseException ex){
            System.out.println("La fecha introducida no cumple el formato");
        }
    }
}
