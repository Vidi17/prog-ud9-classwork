package es.vidal.actividad21;

import java.util.Locale;

public class Validator {

    private final String ZIP_PATTERN = "^([0-4][0-9]|5[0-2])([0-9][0-9][0-9])$";
    private final String NIF_PATTERN = "^(\\d{8})([TRWAGMYFPDXBNJZSQVHLCKE])$";

    public static boolean isValidZipCode(String zip){
        return zip.matches("^([0-4][0-9]|5[0-2])([0-9][0-9][0-9])$");
    }

    public static boolean isValidNIF(String nif){
        return nif.matches("^(\\d{8})([TRWAGMYFPDXBNJZSQVHLCKE])$");
    }

    public static boolean isValidMail(String mail){
        return mail.matches("^([\\w]+\\.?[\\w]+)@([\\w]+-?[\\w]+)\\.(com|es)$");
    }

    public static boolean isValidMAC(String mac){
        mac = mac.toUpperCase(Locale.ROOT);
        return mac.matches("^(\\w\\w):\\w\\w:\\w\\w:\\w\\w:\\w\\w:\\w\\w");
    }

    public static boolean isValidIP(String ip){
        return ip.matches("^(0?\\d?\\d|1?\\d?\\d|2?[0-4]?\\d|2?5?[0-5])\\." +
                "(0?\\d?\\d|1?\\d?\\d|2?[0-4]?\\d|2?5?[0-5])\\." +
                "(0?\\d?\\d|1?\\d?\\d|2?[0-4]?\\d|2?5?[0-5])\\." +
                "(0?\\d?\\d|1?\\d?\\d|2?[0-4]?\\d|2?5?[0-5])$");
    }

    public static boolean hasLength(String string ,int min, int max){
        return (string.length() >= min && string.length() <= max);

    }

    public static boolean isTelephone(String number){
        number = number.replaceAll(" ", "");
        return number.matches("^(0?0?34|\\+34)[67](\\d{8})$");
    }

    public static boolean isNumeric(String number){
        return number.matches("^(PC)\\d\\d");
    }

    public static boolean isNumericBetween(int number, int min, int max){
        return (number >= min && number <= max);
    }
}
