package es.vidal.actividad21;

import java.util.Scanner;

public class TestValidator {

    public static Scanner teclado = new Scanner(System.in);
    public static void main(String[] args) {
        System.out.println("""
        Vamos a introducir los datos del alumno:
        ----------------------------------------""");
        String name = getName();
        String email = getEmail();
        String zip = getZip();
        String telf = getTelephone();
        int age = getAge();
        System.out.println("""
        Vamos a introducir los datos del puesto de trabajo actual:
        ----------------------------------------------------------""");
        String pcName = getPcName();
        String ip = getIP();
        String mac = getMAC();
        System.out.printf("""
        Los datos introducidos son:
        ALUMNO
        nombre: %s
        edad: %d
        email: %s
        movil: %s
        PUESTO DE TRABAJO
        Máquina: %s
        Dirección IP: %s
        Dirección Física: %s""", name, age, email, telf, pcName, ip, mac);
    }

    public static String getName(){
        String name;
        boolean isValid;
        do {
            System.out.print("Introduce el nombre [min=5 max=20]: ");
            name = teclado.nextLine();
            isValid = Validator.hasLength(name, 5, 20);
            if (!isValid){
                System.out.println("Error! Debe introducir un nombre entre 5 y 20 caracteres");
            }
        }while (!isValid);
        return name;
    }

    public static String getEmail(){
        String email;
        boolean isValid;
        do {
            System.out.print("Introduce el correo electrónico: ");
            email = teclado.nextLine();
            isValid = Validator.isValidMail(email);
            if (!isValid){
                System.out.println("Error! Debe introducir un correo válido");
            }
        }while (!isValid);
        return email;
    }

    public static String getZip(){
        String zip;
        boolean isValid;
        do {
            System.out.print("Introduce el código postal: ");
            zip = teclado.nextLine();
            isValid = Validator.isValidZipCode(zip);
            if (!isValid){
                System.out.println("Error! Debe introducir un código postal válido");
            }
        }while (!isValid);
        return zip;
    }

    public static String getTelephone(){
        String telephone;
        boolean isValid;
        do {
            System.out.print("Introduce el número de teléfono móvil: ");
            telephone = teclado.nextLine();
            isValid = Validator.isTelephone(telephone);
            if (!isValid){
                System.out.println("Error! Debe introducir un telefono válido");
            }
        }while (!isValid);
        return telephone;
    }

    public static int getAge(){
        int age;
        boolean isValid;
        do {
            System.out.print("Introduce la edad: ");
            age = teclado.nextInt();
            isValid = Validator.isNumericBetween(age, 0 , 99);
            if (!isValid){
                System.out.println("Error! La edad debe ser numérica entre 0 y 99");
            }
        }while (!isValid);
        return age;
    }

    public static String getMAC(){
        String mac;
        boolean isValid;
        do {
            System.out.print("Introduce la dirección física: ");
            mac = teclado.nextLine();
            isValid = Validator.isValidMAC(mac);
            if (!isValid){
                System.out.println("Error! Debe introducir una dirección física válida");
            }
        }while (!isValid);
        return mac;
    }

    public static String getPcName(){
        String pcName;
        boolean isValid;
        do {
            teclado.nextLine();
            System.out.print("Introduce el código del equipo [PCxx]: ");
            pcName = teclado.nextLine();
            isValid = Validator.isNumeric(pcName);
            if (!isValid){
                System.out.println("Error! El código siempre empieza con PC seguido de la fila y\n" +
                        "columna donde se encuentra");
            }
        }while (!isValid);
        return pcName;
    }

    public static String getIP(){
        String ip;
        boolean isValid;
        do {
            System.out.print("Introduce la dirección de red: ");
            ip = teclado.nextLine();
            isValid = Validator.isValidIP(ip);
            if (!isValid){
                System.out.println("Error! Debe introducir una dirección IP válida");
            }
        }while (!isValid);
        return ip;
    }
}
