package es.vidal.actividad9;

import es.vidal.actividad9.exception.TanqueLlenoException;
import es.vidal.actividad9.exception.TanqueVacioException;

public class TestTanque {
    public static void main(String[] args) {

        Tanque tanque = new Tanque(40);

        try {
            System.out.println("Agrego 51 litros: ");
            tanque.agregarCarga(51);
            System.out.println("Retiro 20 litros: ");
            tanque.retirarCarga(20);
        }catch (TanqueLlenoException | TanqueVacioException ex1){
            System.out.println(ex1.getMessage());
        }

    }
}
