package es.vidal.actividad9;

import es.vidal.actividad9.exception.TanqueLlenoException;
import es.vidal.actividad9.exception.TanqueVacioException;

public class Tanque {

    private final int capacidad;

    private int carga;

    public Tanque(int capacidad) {
        this.capacidad = capacidad;
    }

    public void agregarCarga(int capacidad) throws TanqueLlenoException {
        if (carga + capacidad > this.capacidad){
            throw new TanqueLlenoException();
        }else {
            carga += capacidad;
        }
    }

    public void retirarCarga(int capacidad) throws TanqueVacioException {
        if ((carga - capacidad == 0) || (carga - capacidad < 0)){
            throw new TanqueVacioException();
        }else {
            carga -= capacidad;
        }
    }
}
