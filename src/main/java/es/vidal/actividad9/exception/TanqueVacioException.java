package es.vidal.actividad9.exception;

public class TanqueVacioException extends Exception{

    public TanqueVacioException(){
        super("El tanque está vacío");
    }
}
