package es.vidal.actividad9.exception;

public class TanqueLlenoException extends Exception{

    public TanqueLlenoException() {
        super("El tanque está lleno");
    }
}
