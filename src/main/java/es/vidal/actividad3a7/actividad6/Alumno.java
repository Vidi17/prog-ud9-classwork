package es.vidal.actividad3a7.actividad6;

public class Alumno implements Comparable<Alumno>{

    private String nombre;

    private int edad;

    private double altura;

    public Alumno(String nombre, int edad, double altura) {
        this.nombre = nombre;
        this.edad = edad;
        this.altura = altura;
    }

    public String getNombre() {
        return nombre;
    }

    @Override
    public int compareTo(Alumno alumno) {
        if (alumno.edad > edad){
            return -1;
        }else if (alumno.edad == edad){
            return 0;
        }else {
            return 1;
        }
    }

    @Override
    public String toString(){
      return "Nombre: " + nombre + " Edad: " + edad + " Altura: " + altura + "\n";
    }
}
