package es.vidal.actividad3a7.actividad6;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Actividad6 {

    public static Scanner input = new Scanner(System.in);

    public static void main(String[] args) {

        Alumno[] alumnosDaw = new Alumno[2];

        Alumno alumno;
        for (int i = 0; i < 2; i++) {
            do {
                alumno = crearAlumno();
                input.nextLine();
            }while (alumno == null);
            alumnosDaw[i] = alumno;
        }
        System.out.println(alumnosDaw[0]);
        System.out.println(alumnosDaw[1]);
        decirQueAlumnoEsMasAlto(alumnosDaw);
    }

    public static Alumno crearAlumno(){
        try {
            System.out.println("Introduce el nombre: ");
            String nombre = input.nextLine();
            System.out.println("Introduce la edad: ");
            int edad = input.nextInt();
            System.out.println("Introduce la altura: ");
            double altura = input.nextDouble();
            return new Alumno(nombre, edad, altura);
        }catch (InputMismatchException ex){
            System.out.println("Introduce datos válidos");
            return null;
        }
    }

    public static void decirQueAlumnoEsMasAlto(Alumno[] alumnos) {
        if (alumnos[0].compareTo(alumnos[1]) < 0) {
            System.out.printf("El alumno %s es más mayor", alumnos[1].getNombre());
        } else if (alumnos[0].compareTo(alumnos[1]) == 0) {
            System.out.println("Ambos alumnos tienen la misma edad");
        } else {
            System.out.printf("El alumno %s es más mayor", alumnos[0].getNombre());
        }
    }
}
