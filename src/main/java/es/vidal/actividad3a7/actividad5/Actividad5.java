package es.vidal.actividad3a7.actividad5;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Actividad5 {

    public static Scanner teclado = new Scanner(System.in);

    public static void main(String[] args) {

        int[] numeros1 = new int[5];
        int[] numeros2 = null;

        readNumbers(numeros1, numeros1.length);

        readNumbers(numeros2, numeros1.length);

    }

    public static void readNumbers(int[] numeros, int numerosSize){
        for (int i = 0; i < numerosSize + 1; i++) {
            try {
                System.out.print("Introduzca un número: ");
                numeros[i] = teclado.nextInt();
            }catch (IndexOutOfBoundsException ex){
                System.out.println("Ha sobrepasado el tamaño del array");
                mostrarArray(numeros);
                return;
            }catch (InputMismatchException ex2){
                System.out.println("Ha introducido una letra");
                i--;
            }catch (NullPointerException ex3){
                System.out.println("El array no ha sido inicializado");
                return;
            }finally {
                teclado.nextLine();
            }
        }
    }

    public static void mostrarArray(int[] numeros){
        for (int numero : numeros) {
            System.out.print(numero + ", ");
        }
        System.out.println();
    }
}
