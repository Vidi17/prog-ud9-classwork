package es.vidal.actividad3a7.actividad3;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Actividad3 {

    public static Scanner teclado = new Scanner(System.in);

    public static void main(String[] args) {

        StringBuilder mensaje = new StringBuilder("Numeros Introducidos: ");
        boolean condicionNumero;
        int numeroAnyadir;
        do {
            try {
                numeroAnyadir = obtenerEntero();

            if (numeroAnyadir >= 1 && numeroAnyadir <= 5){
                condicionNumero = true;
                mensaje.replace(mensaje.length() - 2, mensaje.length() - 1, ".");
            }else {
                mensaje.append(numeroAnyadir).append(", ");
                condicionNumero = false;
            }
            }catch (InputMismatchException ex){
                System.out.println("Por favor introduzca un número entero");
                teclado.nextLine();
                condicionNumero = false;
            }
        }while (!condicionNumero);

        System.out.println(mensaje);
    }

    public static int obtenerEntero(){
        System.out.print("Introduce un número: ");
        return teclado.nextInt();
    }
}
