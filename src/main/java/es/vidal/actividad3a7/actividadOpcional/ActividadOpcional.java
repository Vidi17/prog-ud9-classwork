package es.vidal.actividad3a7.actividadOpcional;

import java.util.ArrayList;

public class ActividadOpcional {
    public static void main(String[] args) {
        int numero = 2;

        ArrayList<Integer> numeros = new ArrayList<>();
        numeros.add(-2);
        numeros.add(-1);
        numeros.add(0);
        numeros.add(1);
        numeros.add(2);

        dividirEntreArray(numero, numeros);

    }

    public static void dividirEntreArray(int numero, ArrayList<Integer> enteros){
        for (Integer entero : enteros) {
            try {
                System.out.println(numero / filtroCero(entero));
            }catch (ArithmeticException ex){
                System.out.println("Un número no se puede dividir entre cero");
            }
        }
    }

    public static int filtroCero(int numero){
        if (numero == 0){
            return 1;
        }else {
            return numero;
        }
    }
}
