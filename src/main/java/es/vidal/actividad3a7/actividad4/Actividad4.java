package es.vidal.actividad3a7.actividad4;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Actividad4 {

    public static Scanner teclado = new Scanner(System.in);
    public static void main(String[] args) {

        ArrayList<Integer> numeros = new ArrayList<>();

        boolean condicionNumero;
        do {
            try {
                System.out.println("Introduce una lista de números, escribe una letra para finalizar: ");
                obtenerNumero(numeros);
                System.out.printf("El número más alto es %d", sacarElMaximo(numeros));
                condicionNumero = true;
            }catch (IndexOutOfBoundsException ex){
                System.out.println("Por favor introduce enteros");
                condicionNumero = false;
            }
        }while (!condicionNumero);

    }

    public static void obtenerNumero(ArrayList<Integer> numeros){
        boolean condicionNumero;
        do {
            try {
                numeros.add(teclado.nextInt());
                condicionNumero = true;
            }catch (InputMismatchException ex){
                condicionNumero = false;
                teclado.nextLine();
            }
        }while (condicionNumero);
    }

    public static int sacarElMaximo(ArrayList<Integer> numeros){
        int numero = numeros.get(0);
        for (Integer integer : numeros) {
            if (integer > numero) {
                numero = integer;
            }
        }
        return numero;
    }
}
