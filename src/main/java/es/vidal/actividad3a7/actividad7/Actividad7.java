package es.vidal.actividad3a7.actividad7;

import java.util.ArrayList;

public class Actividad7 {
    public static void main(String[] args) {
        ArrayList<String> cadenas = new ArrayList<>();

        cadenas.add("Manolo");
        cadenas.add("Pepe");
        cadenas.add(null);
        cadenas.add("Jordi");

        mostrarCadenasArray(cadenas);
    }

    public static void mostrarCadenasArray(ArrayList<String> cadenas){
        for (String cadena : cadenas) {
            try {
                System.out.println("Inicial cadena: " + cadena.charAt(0));
            } catch (NullPointerException ex) {
                System.out.println("Esta cadena está vacía");
            }
        }
    }
}
