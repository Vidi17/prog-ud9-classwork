package es.vidal.actividad26;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Actividad26 {
    public static void main(String[] args) {

        LocalDateTime dateTime = LocalDateTime.now();
        System.out.println("Formato ISO_LOCAL_DATE: -> " + dateTime.format(DateTimeFormatter.ISO_LOCAL_DATE));
        System.out.println("Formato ISO_LOCAL_TIME: -> " + dateTime.format(DateTimeFormatter.ISO_LOCAL_TIME));
        System.out.println("Formato ISO_LOCAL_DATE_TIME: -> " + dateTime.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME));
    }
}
