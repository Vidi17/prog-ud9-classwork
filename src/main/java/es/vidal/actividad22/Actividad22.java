package es.vidal.actividad22;

import java.time.LocalDateTime;
import java.util.Scanner;

public class Actividad22 {

    public static Scanner input = new Scanner(System.in);

    public static void main(String[] args) {
        LocalDateTime date = LocalDateTime.now();
        System.out.println("La hora y fecha actual es: " + date);
        System.out.println("""
                Que quieres visualizar:
                1) Dia
                2) Mes
                3) Año
                4) Hora
                5) Minutos""");
        int select = input.nextInt();
        selector(select, date);
    }

    public static void selector(int select, LocalDateTime date){
        switch (select){
            case 1 -> System.out.println("Dia: " + date.getDayOfMonth());
            case 2 -> System.out.println("Mes: " + date.getMonthValue());
            case 3 -> System.out.println("Año: " + date.getYear());
            case 4 -> System.out.println("Hora: " + date.getHour());
            case 5 -> System.out.println("Minuto: " + date.getMinute());
        }
    }
}
