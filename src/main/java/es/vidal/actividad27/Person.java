package es.vidal.actividad27;

import java.time.LocalDate;
import java.time.Period;

public class Person {

    private final int VALID_DAYS = 15;

    private final String firstName;

    private final String lastName;

    private final LocalDate birthday;

    private final String email;

    private final String phoneNumber;

    private final LocalDate createdOn = LocalDate.now();

    private final String password;

    private final String salt;

    public Person(String firstName, String lastName, LocalDate birthday, String email, String phoneNumber, String password, String salt) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthday = birthday;
        this.email = email;
        this.phoneNumber = phoneNumber;
        this.password = password;
        this.salt = salt;
    }

    public String getFirstName() {
        return firstName;
    }

    public int getAge(){
        return Period.between(birthday, LocalDate.now()).getYears();
    }

    public boolean isNewUser(){
        return (Period.between(createdOn, LocalDate.now()).getDays() < VALID_DAYS);
    }
}
