package es.vidal.actividad27;

import java.time.LocalDate;

public class TestPerson {
    public static void main(String[] args) {
        LocalDate birthday = LocalDate.parse("2002-02-22");
        Person person = new Person("Jordi", "Vidal", birthday, "vidalcerda17@gmail.com",
                "695725578", "caca", "Salt");

        System.out.printf("La edad de %s es de %d años\n", person.getFirstName(), person.getAge());
        boolean isNewUser = person.isNewUser();
        if (isNewUser){
            System.out.printf("El usuario %s aún es novato", person.getFirstName());
        }else {
            System.out.printf("El usuario %s ya no es novato", person.getFirstName());
        }
    }
}
