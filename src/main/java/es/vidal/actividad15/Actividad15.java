package es.vidal.actividad15;

import java.util.Scanner;

public class Actividad15 {

    public static Scanner teclado = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("Introduce el número de la cuenta: ");
        System.out.println(checkNumeroCuenta(teclado.nextLine()));

    }

    public static boolean checkNumeroCuenta(String numero){
        return numero.matches("\\d{4}-\\d{4}-\\d{2}-\\d{10}");
    }
}
