package es.vidal.actividad20;

public class Actividad20 {
    public static void main(String[] args) {
        String telefono = "0034695725578";
        System.out.println(checkTelefonoMovil(telefono));

    }

    public static boolean checkTelefonoMovil(String string){
        return string.matches("^(0034|\\+34|34)[67](\\d{8})$");
    }
}
