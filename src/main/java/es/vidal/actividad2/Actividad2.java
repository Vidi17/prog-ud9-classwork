package es.vidal.actividad2;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Actividad2 {

    public static Scanner teclado = new Scanner(System.in);
    public static void main(String[] args) {

        ArrayList<Integer> numeros = new ArrayList<>();

        for (int i = 0; i < 6; i++) {
            try {
                numeros.add(obtenerEntero(i));
            }catch (InputMismatchException ex){
                System.out.println("Debe ingresar obligatoriamente un número entero");
                teclado.nextLine();
                i--;
            }
        }
        System.out.printf("El número más alto es %d", sacarElMaximo(numeros));
    }

    public static int obtenerEntero(int i){
        System.out.printf("Introduce el número %d: ", i + 1);
        return teclado.nextInt();
    }

    public static int sacarElMaximo(ArrayList<Integer> numeros){
        int numero = numeros.get(0);
        for (Integer integer : numeros) {
            if (integer > numero) {
                numero = integer;
            }
        }
        return numero;
    }
}
