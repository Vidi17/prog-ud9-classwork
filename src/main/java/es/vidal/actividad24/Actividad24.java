package es.vidal.actividad24;

import java.time.LocalTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

public class Actividad24 {
    public static void main(String[] args) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm:ss");

        ZoneId MADRID_EUROPEAN_TIME = ZoneId.of("Europe/Madrid");
        ZoneId CAIRO_AFRICAN_TIME = ZoneId.of("Africa/Cairo");
        ZoneId LONDON_EUROPEAN_TIME = ZoneId.of("Europe/London");

        System.out.print("La hora actual de Madrid es : ");
        System.out.println(LocalTime.now(MADRID_EUROPEAN_TIME).format(formatter));
        System.out.print("La hora actual de El Cairo es : ");
        System.out.println(LocalTime.now(CAIRO_AFRICAN_TIME).format(formatter));
        System.out.print("La hora actual de Londres es : ");
        System.out.println(LocalTime.now(LONDON_EUROPEAN_TIME).format(formatter));
    }
}
