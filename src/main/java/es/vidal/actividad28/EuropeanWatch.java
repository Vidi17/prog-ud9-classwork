package es.vidal.actividad28;

import java.time.*;
import java.time.format.DateTimeFormatter;

public class EuropeanWatch {

    private final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
    private final ZoneId WESTERN_EUROPEAN_TIME = ZoneId.of("Europe/Dublin");
    private final ZoneId CENTRAL_EUROPEAN_TIME = ZoneId.of("Europe/Paris");
    private final ZoneId EASTERN_EUROPEAN_TIME = ZoneId.of("UTC");

    public void showTime(Capitals capital){
        switch(capital){
            case BUDAPEST, LA_VALETA, LUXEMBURGO, AMSTERDAM, ESTOCOLMO ->
                    System.out.println(LocalDateTime.now(CENTRAL_EUROPEAN_TIME).format(FORMATTER));
            case DUBLIN -> System.out.println(LocalDateTime.now(WESTERN_EUROPEAN_TIME).format(FORMATTER));
            case REYKJAVIK -> System.out.println(LocalDateTime.now(EASTERN_EUROPEAN_TIME).format(FORMATTER));
        }
    }
}
