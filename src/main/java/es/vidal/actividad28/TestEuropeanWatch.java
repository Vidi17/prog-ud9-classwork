package es.vidal.actividad28;

public class TestEuropeanWatch {
    public static void main(String[] args) {

        EuropeanWatch europeanWatch = new EuropeanWatch();

        System.out.print("La fecha y hora actual de Budapest es : ");
        europeanWatch.showTime(Capitals.BUDAPEST);
        System.out.print("La fecha y hora actual de La Valeta es : ");
        europeanWatch.showTime(Capitals.LA_VALETA);
        System.out.print("La fecha y hora actual de Luxemburgo es : ");
        europeanWatch.showTime(Capitals.LUXEMBURGO);
        System.out.print("La fecha y hora actual de Amsterdam es : ");
        europeanWatch.showTime(Capitals.AMSTERDAM);
        System.out.print("La fecha y hora actual de Dublin es : ");
        europeanWatch.showTime(Capitals.DUBLIN);
        System.out.print("La fecha y hora actual de Estocolmo es : ");
        europeanWatch.showTime(Capitals.ESTOCOLMO);
        System.out.print("La fecha y hora actual de Reykjavik es : ");
        europeanWatch.showTime(Capitals.REYKJAVIK);

    }
}
