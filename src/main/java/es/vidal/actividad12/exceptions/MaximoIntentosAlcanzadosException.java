package es.vidal.actividad12.exceptions;

public class MaximoIntentosAlcanzadosException extends RuntimeException{

    public MaximoIntentosAlcanzadosException() {
        super("Se ha alcanzado el número máximo de intentos");
    }
}
