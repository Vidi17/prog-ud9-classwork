package es.vidal.actividad12.exceptions;

public class CredencialesInvalidasException extends RuntimeException{

    public CredencialesInvalidasException(String message) {
        super(message);
    }

    public CredencialesInvalidasException() {
        super("El usuario existe pero la contraseña no coincide");
    }
}
