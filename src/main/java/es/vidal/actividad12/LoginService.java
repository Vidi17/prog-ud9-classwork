package es.vidal.actividad12;

import java.util.HashMap;
import java.util.Scanner;
import es.vidal.actividad12.exceptions.CredencialesInvalidasException;
import es.vidal.actividad12.exceptions.MaximoIntentosAlcanzadosException;

public class LoginService {

    public Scanner teclado = new Scanner(System.in);

    private final int NUMERO_MAXIMO_INTENTOS = 5;

    private int intentos = 0;

    private final HashMap<String, String> usuarios = new HashMap<>();

    public LoginService(){
        usuarios.put("Pedro", "Lacasa");
        usuarios.put("Carlos", "1234");
        usuarios.put("Manolo", "2134");
        usuarios.put("Jordi", "2002");
    }

    public String obtenerUsuario(){
        System.out.print("Introduce el Usuario: ");
        return teclado.next();
    }

    public void reiniciarIntentos(){
        intentos = 0;
    }

    public boolean tieneIntentosPendientes(){
        return (intentos == NUMERO_MAXIMO_INTENTOS);
    }

    public void comprobarCredenciales() throws CredencialesInvalidasException, MaximoIntentosAlcanzadosException{
        if (intentos == NUMERO_MAXIMO_INTENTOS){
            throw new MaximoIntentosAlcanzadosException();
        }
        intentos++;

        String usuario = obtenerUsuario();
        System.out.print("Introduce la contraseña: ");
        String contrasenya = teclado.next();

        if (!usuarios.containsKey(usuario)){
            throw new CredencialesInvalidasException("El usuario no existe");
        }
        if (!usuarios.containsValue(contrasenya)){
            throw new CredencialesInvalidasException();
        }
    }
}
