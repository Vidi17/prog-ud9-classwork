package es.vidal.actividad12;

import es.vidal.actividad12.exceptions.CredencialesInvalidasException;
import es.vidal.actividad12.exceptions.MaximoIntentosAlcanzadosException;

public class TestLoginService {
    public static void main(String[] args) {
        LoginService loginService = new LoginService();
        boolean haIniciadoSesion = false;

        do {
            try {
                loginService.comprobarCredenciales();
                System.out.println("Inicio de sesión correcto");
                haIniciadoSesion = true;
            }catch (CredencialesInvalidasException ex1){
                System.out.println(ex1.getMessage());
            }catch (MaximoIntentosAlcanzadosException ex2){
                System.out.println(ex2.getMessage());
                haIniciadoSesion = true;
            }
        }while (!haIniciadoSesion);
    }
}
