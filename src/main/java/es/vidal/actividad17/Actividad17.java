package es.vidal.actividad17;

import java.util.Scanner;

public class Actividad17 {

    public static Scanner teclado = new Scanner(System.in);

    public static void main(String[] args) {
    System.out.println("Introduce tu nif: ");
    System.out.println(checkNif(teclado.nextLine()));
    }

    public static boolean checkNif(String nif){
        return nif.matches("^(\\d{8})([TRWAGMYFPDXBNJZSQVHLCKE])$");
    }
}
